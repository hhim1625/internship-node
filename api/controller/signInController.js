'use strict';
var mongoose = require('mongoose'),
    userAssembler = require('../assembler/userAssembler.js'),   
    User=mongoose.model('user'),
    md5 = require('md5');

exports.signInUser = function(req, res) {  
    mongoose.connect(process.env.MONGO_URL || "mongodb://localhost/car-db", {useNewUrlParser: true});
    User.findOne({email: req.body.email}, function(err, actUser){
        if(err) {
            res.status(500).send(err);
        }
        else if(actUser) {
            if (actUser.password===md5(req.body.password)) {
                    res.json({
                        id:actUser._id,
                        username:actUser.username, 
                        email:actUser.email,    
                		role:actUser.role});
            }
            else {
           	   res.sendStatus(401);
            }
        } else {
           	   res.sendStatus(404);
        }
    });
};

exports.addUser = function(req, res) {
    User.findOne({email: req.body.email}, function(err, actUser){
        if(err) {
            res.status(500).send(err);
        } else if(actUser) {
            res.status(400).send({message:"Email already exists!"});
        } else {
            const user = new User(userAssembler.dtoToModel(req.body));
            user.password = md5(user.password);
            user.validate(function(err) {
            if (err) {
                res.status(400).send({message: err.message});
            } else {
                user.save(function(err, user) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        res.json(userAssembler.modelToDto(user));
                    }
                });
            }
        });
        }
    });
};