'use strict';
var mongoose = require('mongoose'),
    userAssembler = require('../assembler/userAssembler.js'),   
    User=mongoose.model('user');

exports.findAllUsers = function (req, res) {
    User.find({}, function (err, users) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json(userAssembler.modelsToDtos(users));
        }
    });
};

exports.deleteUser = function(req, res) {
    User.findById(req.params.userId, function(err, user) {
        if (err) {
            res.status(500).send(err);
        } else if (user === null) {
            res.sendStatus(404);
        } else {
        	if (user.role === "user" || user.role === "User"){
	            user.remove(function(err) {
	                if (err) {
	                    res.status(500).send(err);
	                } else {
	                    res.json({ message: 'User successfully deleted' });
	                }
	            });
            } else {
               res.sendStatus(401).send({message:"You cannot delete an admin"});
            }
        }
    });
};