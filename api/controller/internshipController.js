'use strict';

var mongoose = require('mongoose'),
    internshipAssembler = require('../assembler/internshipAssembler.js'),
    Internship = mongoose.model('internship');

exports.findAllInternships = function (req, res) {
    Internship.find({}, function (err, internships) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json(internshipAssembler.modelsToDtos(internships));
        }
    });
};

exports.findInternshipById = function (req, res) {
    Internship.findById(req.params.internshipId, function (err, internship) {
        if (err) {
            res.status(500).send(err);
        } else if (internship === null) {
            res.sendStatus(404);
        } else {
            res.json(internshipAssembler.modelToDto(internship));
        }
    });
};

exports.createInternship = function(req, res) {
    const internship = new Internship(internshipAssembler.dtoToModel(req.body));
    internship.validate(function(err) {
        if (err) {
            res.status(400).send({message: err.message});
        } else {
            internship.save(function(err, internship) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json(internshipAssembler.modelToDto(internship));
                }
            });
        }
    });
};

exports.deleteInternship = function(req, res) {
    Internship.findById(req.params.internshipId, function(err, internship) {
        if (err) {
            res.status(500).send(err);
        } else if (internship === null) {
            res.sendStatus(404);
        } else {
            internship.remove(function(err) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json({ message: 'Internship successfully deleted' });
                }
            });
        }
    });
};

exports.updateInternship = function(req, res) {
    const newInternship = new Internship(internshipAssembler.dtoToModel(req.body));
    Internship.findById(req.params.internshipId, function(err, internship){
        if (err) {
            res.status(500).send(err);
        } else if ( internship === null ) {
            res.sendStatus(404);
        } else {
            internship.remove(function(err) {
                if (err) {
                    res.status(500).send(err);
                } else {

                }
            });
            newInternship.save(function(err, newInternship) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json(internshipAssembler.modelToDto(newInternship));
                }
            });
        }
    });
};
