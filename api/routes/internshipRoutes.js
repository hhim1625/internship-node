'use strict';

var express = require('express');
var internshipController = require('../controller/internshipController.js');
var router = express.Router();

router.route('/internships')
    .get(internshipController.findAllInternships)
    .post(internshipController.createInternship);

router.route('/internships/:internshipId')
    .get(internshipController.findInternshipById)
    .put(internshipController.updateInternship)
    .delete(internshipController.deleteInternship);

module.exports = router;
