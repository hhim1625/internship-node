var express = require("express");
var router = express.Router();
var signInController = require('../controller/signInController');

router.route('/signIn')
    .put(signInController.addUser)
    .post(signInController.signInUser);


module.exports = router;
