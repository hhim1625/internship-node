 var express = require("express");
var router = express.Router();
var userController = require('../controller/userController');

router.route('/users')
    .get(userController.findAllUsers);
    
 router.route('/users/:userId')
    .delete(userController.deleteUser);

module.exports = router;
