'use strict';

var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    address: {
        type: String
    },
    phonenumber: {
        type: Number
    },
    role: {
    	type: String,
    	required: true
    },
    email: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('user', UserSchema);
