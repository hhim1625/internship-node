'use strict';

var mongoose = require('mongoose');

var InternshipSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    theme: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    owner: {
        type: String,
        required: true
    },
    fromDate: {
        type: Date,
        required: true
    },
    toDate: {
        type: Date,
        required: true
    },
    comapnyName: {
        type: String
    },
    city: {
        type: String
    }
});

module.exports = mongoose.model('internship', InternshipSchema);
