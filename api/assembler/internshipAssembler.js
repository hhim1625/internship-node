exports.modelToDto = function (internship) {
    return {
        id: internship._id,
        name: internship.name,
        theme: internship.theme,
        description: internship.description,
        owner: internship.owner,
        fromDate: internship.fromDate,
        toDate: internship.toDate,
        comapanyName: internship.comapanyName,
        city: internship.city
    };
};

exports.dtoToModel = function (internshipDto) {
    return {
        _id: internshipDto.id,
        name: internshipDto.name,
        theme: internshipDto.theme,
        description: internshipDto.description,
        owner: internshipDto.owner,
        fromDate: internshipDto.fromDate,
        toDate: internshipDto.toDate,
        comapanyName: internshipDto.comapanyName,
        city: internshipDto.city
    };
};

exports.modelsToDtos = function(internships){
    return internships.map(x => exports.modelToDto(x));
};

exports.dtosToModels = function (internshipDtos) {
    return internshipDtos.map(x => exports.dtoToModel(x));
};
