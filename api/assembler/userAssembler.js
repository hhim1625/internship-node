exports.modelToDto = function (user) {
    return {
        id: user._id,
        username: user.username,
        password: user.password,
        address: user.address,
        phonenumber: user.phonenumber,
        role: user.role,
        email: user.email
    };
};

exports.dtoToModel = function (userDto) {
    return {
        _id: userDto.id,
        username: userDto.username,
        password: userDto.password,
        address: userDto.address,
        phonenumber: userDto.phonenumber,
        role: userDto.role,
        email: userDto.email
    };
};

exports.modelsToDtos = function(users){
    return users.map(x => exports.modelToDto(x));
};

exports.dtosToModels = function (userDtos) {
    return userDtos.map(x => exports.dtoToModel(x));
};
