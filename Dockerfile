FROM node:latest

RUN mkdir -p /usr/src/server

WORKDIR /usr/src/server

COPY . .

RUN npm install

CMD ["npm", "start"]
