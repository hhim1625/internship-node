var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    cors = require('cors');

var port = process.env.PORT || 3001,
    mongoUrl = process.env.MONGO_URL || 'mongodb://localhost/internship-db';

mongoose.Promise = global.Promise;
mongoose.connect(mongoUrl, { useNewUrlParser: true }, function(err) {
    if (err) {
    	console.log("Mongo not available, exiting");
        process.exit(1);
    }
    console.log("Connection successfull!")
});

var app = express(); 
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require('./api/schema/userSchema.js');
require('./api/schema/internshipSchema.js');
var internshipRoutes = require('./api/routes/internshipRoutes');
var signInRoutes = require('./api/routes/signInRoutes');
var userRoutes = require('./api/routes/userRoutes');
app.use('/api', internshipRoutes);
app.use('/api', signInRoutes);
app.use('/api', userRoutes);
app.listen(port, function() {
	console.log('Internship RESTful API server started on port ' + port);
});

module.exports = app;